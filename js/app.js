let w = window.innerWidth;
console.log("Jelenlegi képernyőméret szélessége: ", w);
let h = window.innerHeight;
console.log("Jelenlegi képernyőméret hossza: ", h);

const body = document.querySelector("body");
const navbar = document.querySelector(".navbar");
const menuBtn = document.querySelector(".menu-btn");
const cancelBtn = document.querySelector(".cancel-btn");

menuBtn.onclick = () => {
  navbar.classList.add("show");
  menuBtn.classList.add("hide");
  body.classList.add("disabled");
}
cancelBtn.onclick = () => {
  body.classList.remove("disabled");
  navbar.classList.remove("show");
  menuBtn.classList.remove("hide");
}

function menuClicked() {
  body.classList.remove("disabled");
  navbar.classList.remove("show");
  menuBtn.classList.remove("hide");
}

window.onscroll = () => {
  this.scrollY > 20 ? navbar.classList.add("sticky") : navbar.classList.remove("sticky");
}

/* Modals */
let projectLinks = document.getElementsByClassName("project-link");
let modals = document.querySelectorAll('.modal');
let index = 1;

function openModal(index) {
  if (index > modals.length) { index = 1 }
  if (index < 1) { index = modals.length }
  for (let i = 0; i < modals.length; i++) {
    modals[i].style.display = "none";
  }
  projectLinks[index - 1].onclick = () => {
    modals[index - 1].style.display = "block";
    console.log(index);
  }
}

function closeModal() {
  modals.forEach(m => {
    m.style.display = "none";
  });
}

$(document).ready(function () {
  $(window).scroll(function () {
    console.log(this.scrollY);
    if (this.scrollY > 400 || this.scrollY > 1400) {
      $("#about > div.row.about-content > div.col-4.about-card-profile > div").addClass("zoomIn animated-slow-2");
      $("#about > div.row.about-content > div.row > div > h2").addClass("fadeInUp animated-slow-2 animation-delay-10");
      $("#about > div.row.about-content > div.row > div > div").addClass("width30 animated-slow-3 animation-delay-1");
      $("#about > div.row.about-content > div.row > div > div").addClass("width50 animated-slow-3 animation-delay-1");
    }
    if (this.scrollY > 1400 || this.scrollY > 2600) {
      $(".svg").addClass("zoomIn animated-slow-2");
    }
    if (this.scrollY > 2200 || this.scrollY > 1400 || this.scrollY > 3400) {
      $(".projects-content > div:nth-child(1) > div:nth-child(1)").addClass("fadeIn animation-delay-03");
      $(".projects-content > div:nth-child(1) > div:nth-child(2)").addClass("fadeIn animation-delay-05");
      $(".projects-content > div:nth-child(1) > div:nth-child(3)").addClass("fadeIn animation-delay-07");
    }
    if (this.scrollY > 2500 || this.scrollY > 2000 || this.scrollY > 4000) {
      $(".projects-content > div:nth-child(2) > div:nth-child(1)").addClass("fadeIn animation-delay-03");
      $(".projects-content > div:nth-child(2) > div:nth-child(2)").addClass("fadeIn animation-delay-05");
      $(".projects-content > div:nth-child(2) > div:nth-child(3)").addClass("fadeIn animation-delay-07");
    }
    if (this.scrollY > 2800 || this.scrollY > 2200 || this.scrollY > 4600) {
      $(".projects-content > div:nth-child(3) > div:nth-child(1)").addClass("fadeIn animation-delay-03");
      $(".projects-content > div:nth-child(3) > div:nth-child(2)").addClass("fadeIn animation-delay-05");
    }
    if (this.scrollY > 3500 || this.scrollY > 5500) {
      $("#contact > div > div > div > div.card-title > h2").addClass("fadeInUp animated-slow-2");
      $("#contact > div > div > div > div.card-title > div").addClass("width90 animated-slow-2");
    }
  });
});

